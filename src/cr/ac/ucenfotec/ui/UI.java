package cr.ac.ucenfotec.ui;

import cr.ac.ucenfotec.bl.Computadora;
import cr.ac.ucenfotec.bl.Empleado;

public class UI {

    public static void main(String[] args) {
        Empleado empleado1 = new Empleado("123","Juan");
        Empleado empleado2 = new Empleado("456","Ana");

        Computadora compu1 = new Computadora("SE-001","Dell");
        compu1.setResponsable(empleado1);

        Computadora compu2 = new Computadora("SE-002","HP",empleado2);

        System.out.println("Info Compu1:  " + compu1);
        System.out.println("Info Compu2:  " + compu2);
    }
}
